
export function wrapInputExpectedResult<T, U>(input: T, expectedResult: U) {
    return {
        input,
        expectedResult
    };
}