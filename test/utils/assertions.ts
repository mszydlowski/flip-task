import {assert} from 'chai';
import {CustomException} from '../../src/error/CustomException';

export async function shouldThrowAsync<T>(asyncFunction: () => Promise<T>, expectedError: Error | CustomException): Promise<void> {
    try {
        await asyncFunction();
        assert.fail();
    } catch (error) {
        assert.equal(error.toString(), expectedError.toString());
    }
}

export async function shouldNotThrowErrorAsync<T>(asyncFunction: () => Promise<T>): Promise<void> {
    try {
        await asyncFunction();
    } catch (error) {
        assert.fail();
    }
}