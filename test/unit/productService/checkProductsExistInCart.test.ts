import {createSandbox, SinonSandbox, SinonStub} from 'sinon';
import * as cartRepo from '../../../src/db/repository/cartRepo';
import {shouldThrowAsync, shouldNotThrowErrorAsync} from '../../utils/assertions';
import {CustomException} from '../../../src/error/CustomException';
import {ErrorMessages} from '../../../src/error/ErrorMessages';
import {checkProductsExistInCart} from '../../../src/core/services/productService';

describe('Product service - checkProducstExistInCart', () => {

    let sandbox: SinonSandbox;
    let getCartByIdStub: SinonStub;

    beforeEach(() => {
        sandbox = createSandbox();
        getCartByIdStub = sandbox.stub(cartRepo, 'getCartById');
    });

    afterEach(() => {
        sandbox.restore();
    });

    const testCart = {
        _id: 'sampleId',
        products: [
            {
                productId: 'one',
                quantity: 2
            },
            {
                productId: 'two',
                quantity: 3
            }
        ]
    };

    it('should not throw any error if products exist in cart', async () => {
        getCartByIdStub.resolves(testCart);
        const wrapperFn = async () => checkProductsExistInCart(testCart._id, [
            {
                productId: 'one',
                quantity: 2
            },
            {
                productId: 'two',
                quantity: 3
            }
        ]);
        await shouldNotThrowErrorAsync(wrapperFn);
    });

    it('should throw productsNotInCart error if products do not exist in cart', async () => {
        getCartByIdStub.resolves(testCart);
        const wrapperFn = async () => checkProductsExistInCart(testCart._id, [
            {
                productId: 'three',
                quantity: 2
            },
            {
                productId: 'two',
                quantity: 3
            }
        ]);
        await shouldThrowAsync(wrapperFn, new CustomException(ErrorMessages.productsNotInCart));
    });


});