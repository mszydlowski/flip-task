import {createSandbox, SinonSandbox, SinonStub} from 'sinon';
import * as productRepo from '../../../src/db/repository/productRepo';
import {shouldThrowAsync, shouldNotThrowErrorAsync} from '../../utils/assertions';
import {CustomException} from '../../../src/error/CustomException';
import {ErrorMessages} from '../../../src/error/ErrorMessages';
import {checkProductsExistInDesiredQuantities} from '../../../src/core/services/productService';

describe('Product service - checkProducstExistInCart', () => {

    let sandbox: SinonSandbox;
    let findProductsByIdsStub: SinonStub;

    beforeEach(() => {
        sandbox = createSandbox();
        findProductsByIdsStub = sandbox.stub(productRepo, 'findProductsByIds');
    });

    afterEach(() => {
        sandbox.restore();
    });

    const testProducts = [
        {
            _id: 'one',
            quantity: 8,
            frozenQuantity: 6
        },
        {
            _id: 'two',
            quantity: 5,
            frozenQuantity: 2
        }
    ];


    it('should not throw any error if products exist in selected quantities', async () => {
        findProductsByIdsStub.resolves(testProducts);
        const wrapperFn = async () => checkProductsExistInDesiredQuantities([
            {
                productId: 'one',
                quantity: 2
            },
            {
                productId: 'two',
                quantity: 3
            }
        ]);
        await shouldNotThrowErrorAsync(wrapperFn);
    });

    it('should throw productsNotInCart error if products do not exist', async () => {
        findProductsByIdsStub.resolves([{
            _id: 'two',
            quantity: 5,
            frozenQuantity: 2
        }]);
        const wrapperFn = async () => checkProductsExistInDesiredQuantities([
            {
                productId: 'three',
                quantity: 2
            },
            {
                productId: 'two',
                quantity: 3
            }
        ]);
        await shouldThrowAsync(wrapperFn, new CustomException(ErrorMessages.noSuchProduct));
    });


    it('should throw productsNotInCart error if products are in insufficient amounts', async () => {
        findProductsByIdsStub.resolves(testProducts);
        const wrapperFn = async () => checkProductsExistInDesiredQuantities([
            {
                productId: 'one',
                quantity: 5
            },
            {
                productId: 'two',
                quantity: 3
            }
        ]);
        await shouldThrowAsync(wrapperFn, new CustomException(ErrorMessages.productsNotAvailableInThatAmount));
    });


});