import {createSandbox, SinonSandbox, SinonStub} from 'sinon';
import {assert} from 'chai';
import {wrapInputExpectedResult} from '../../utils/wrapInputExpectedResult';
import {CartProduct} from '../../../src/core/businessEntities/CartProduct';
import {groupProductsById} from '../../../src/core/services/productService';

describe('Product service - groupProductsById', () => {


    [
        wrapInputExpectedResult<CartProduct[], CartProduct[]>([
            {
                productId: 'one',
                quantity: 4
            }, {
                productId: 'one',
                quantity: 7
            }, {
                productId: 'two',
                quantity: 5
            }
        ], [
            {
                productId: 'one',
                quantity: 11
            }, {
                productId: 'two',
                quantity: 5
            }
        ]),
        wrapInputExpectedResult([], [])
    ].forEach((item) => {
        it('should group products by ID, summing input quantities', async () => {
            const groupedProducts = groupProductsById(item.input);
            assert.deepStrictEqual(groupedProducts, item.expectedResult);
        });
    });


});