import {createSandbox, SinonSandbox, SinonStub} from 'sinon';
import * as cartRepo from '../../../src/db/repository/cartRepo';
import * as currencyService from '../../../src/core/services/currencyService';
import {calculateCheckout} from '../../../src/core/services/cartService';
import {assert} from 'chai';
import {wrapInputExpectedResult} from '../../utils/wrapInputExpectedResult';

describe('Cart service - calculateCheckout', () => {

    let sandbox: SinonSandbox;
    let getCartByIdWithProductsStub: SinonStub;
    let getCurrencyDataStub: SinonStub;

    const testCartId = 'testCartId';

    beforeEach(() => {
        sandbox = createSandbox();
        getCartByIdWithProductsStub = sandbox.stub(cartRepo, 'getCartByIdWithProducts');
        getCurrencyDataStub = sandbox.stub(currencyService, 'getCurrencyData');
    });

    afterEach(() => {
        sandbox.restore();
    });

    [
        wrapInputExpectedResult([
            {
                productId: {
                    _id: 'idOne',
                    price: {
                        currency: 'EUR',
                        value: 20
                    },
                    name: 'one'
                },
                quantity: 3
            },
            {
                productId: {
                    _id: 'idTwo',
                    price: {
                        currency: 'USD',
                        value: 60
                    },
                    name: 'two'
                },
                quantity: 2
            }
        ], [
            {
                _id: 'idOne',
                name: 'one',
                quantity: 3,
                originalCurrency: 'EUR',
                totalPriceInOriginalCurrency: '60.00',
                totalPriceInTargetCurrency: '60.00',
                retailPriceInOriginalCurrency: '20.00',
                retailPriceInTargetCurrency: '20.00',
                exchangeRateEURToTarget: 1,
                exchangeRateOriginalToEUR: 1

            },
            {
                _id: 'idTwo',
                name: 'two',
                quantity: 2,
                originalCurrency: 'USD',
                totalPriceInOriginalCurrency: '120.00',
                totalPriceInTargetCurrency: '80.00',
                retailPriceInOriginalCurrency: '60.00',
                retailPriceInTargetCurrency: '40.00',
                exchangeRateEURToTarget: 1,
                exchangeRateOriginalToEUR: 1.5
            }
        ]),
        wrapInputExpectedResult([], [])
    ].forEach((item) => {
        it('should calculate checkout for a given cart', async () => {
            getCartByIdWithProductsStub.resolves({
                _id: 'sampleId',
                products: item.input
            });
            getCurrencyDataStub.resolves({
                rates: {
                    USD: 1.5
                }
            });
            const checkoutData = await calculateCheckout(testCartId, 'EUR');
            assert.deepStrictEqual(checkoutData, item.expectedResult);
        });
    });


});