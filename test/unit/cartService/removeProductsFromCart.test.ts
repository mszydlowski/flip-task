
import {createSandbox, SinonSandbox, SinonStub} from 'sinon';
import * as productRepo from '../../../src/db/repository/productRepo';
import * as cartRepo from '../../../src/db/repository/cartRepo';
import {removeProductsFromCart} from '../../../src/core/services/cartService';
import {assert} from 'chai';
import {wrapInputExpectedResult} from '../../utils/wrapInputExpectedResult';
import {CartProduct} from '../../../src/core/businessEntities/CartProduct';
import {shouldThrowAsync} from '../../utils/assertions';
import {CustomException} from '../../../src/error/CustomException';
import {ErrorMessages} from '../../../src/error/ErrorMessages';

describe('Cart service - removeProductsFromCart', () => {

    let sandbox: SinonSandbox;
    let bulkWriteStub: SinonStub;
    let getCartByIdStub: SinonStub;
    let updateProductDataStub: SinonStub;

    const testCart = {
        _id: 'testCartId',
        products: [
            {
                productId: 'productOne',
                quantity: 5
            },
            {
                productId: 'productTwo',
                quantity: 3
            },
            {
                productId: 'productThree',
                quantity: 9
            }
        ]
    };

    beforeEach(() => {
        sandbox = createSandbox();
        bulkWriteStub = sandbox.stub(productRepo, 'bulkIncrement');
        getCartByIdStub = sandbox.stub(cartRepo, 'getCartById');
        updateProductDataStub = sandbox.stub(cartRepo, 'updateProductData');
    });

    afterEach(() => {
        sandbox.restore();
    });

    [
        wrapInputExpectedResult<CartProduct[], CartProduct[]>([
            {
                productId: 'productOne',
                quantity: 2
            }, {
                productId: 'productThree',
                quantity: 4
            }
        ], [
            {
                productId: 'productOne',
                quantity: 3
            }, {
                productId: 'productTwo',
                quantity: 3
            }, {
                productId: 'productThree',
                quantity: 5
            }
        ]),
        wrapInputExpectedResult<CartProduct[], CartProduct[]>([
            {
                productId: 'productOne',
                quantity: 5
            },
            {
                productId: 'productTwo',
                quantity: 1
            }
        ], [
            {
                productId: 'productTwo',
                quantity: 2
            }, {
                productId: 'productThree',
                quantity: 9
            }
        ]),
        wrapInputExpectedResult<CartProduct[], CartProduct[]>([

        ], [
            {
                productId: 'productOne',
                quantity: 5
            },
            {
                productId: 'productTwo',
                quantity: 3
            },
            {
                productId: 'productThree',
                quantity: 9
            }
        ])
    ].forEach((item) => {
        it('should remove desired products from a selected cart', async () => {
            bulkWriteStub.resolves(null);
            getCartByIdStub.resolves(testCart);
            updateProductDataStub.resolves(null);
            await removeProductsFromCart(testCart._id, item.input);
            assert.deepStrictEqual(updateProductDataStub.firstCall.args[0], testCart._id);
            assert.deepStrictEqual(updateProductDataStub.firstCall.args[1], item.expectedResult);
        });
    });


    it('should rollback changes and throw an error if any stage fails', async () => {
        bulkWriteStub.resolves(null);
        getCartByIdStub.resolves(testCart);
        updateProductDataStub.throws(new Error());
        const wrapperFn = async () => removeProductsFromCart(testCart._id, []);
        await shouldThrowAsync(wrapperFn, new CustomException(ErrorMessages.errorRemovingFromCart));
    });


});