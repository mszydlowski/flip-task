import {Application, Request, Response} from 'express';
import * as fs from 'fs';
import * as morgan from 'morgan';
import {healthcheckUrl} from '../http/urls';

import * as winston from 'winston';

const {combine, timestamp, prettyPrint, json, splat} = winston.format;
const logsDir = 'logs';

function createLogsFolder(): void {
    if (!fs.existsSync(logsDir)) {
        fs.mkdirSync(logsDir);
    }
}

function createLogger(): winston.Logger {
    createLogsFolder();
    const usedTransports = [
        new winston.transports.File({
            level: 'info',
            filename: './logs/all-logs.log',
            handleExceptions: true,
            maxsize: 5242880, //5MB
            maxFiles: 5
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true
        })];

    const exceptionHandlers = usedTransports;

    return winston.createLogger({
        format: combine(
            timestamp(),
            prettyPrint(),
            json(),
            splat()
        ),
        transports: usedTransports,
        exceptionHandlers,
        exitOnError: false
    });
}


export const logger = createLogger();

export function createMorganMiddleware(app: Application): void {
    createLogsFolder();
    app.use(morgan('combined', {
        stream: {
            write(message: string): void {
                logger.info(message);
            }
        },
        // skip(req: Request, res: Response): boolean {
        //     return req.url === healthcheckUrl;
        // }
    }));
}

