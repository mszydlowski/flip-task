
import * as  http from 'http';
import {OK} from 'http-status-codes';


/* eslint-disable unicorn/no-process-exit */

const options = {
    host: 'localhost',
    path: '/health/ping',
    port: process.env.PORT,
    timeout: 2000,
};

const request = http.request(options, (res) => {
    // eslint-disable-next-line no-console
    console.info(`STATUS: ${res.statusCode}`);
    if (res.statusCode === OK) {
        process.exit(0);
    } else {
        process.exit(1);
    }
});

request.on('error', function(err: Error): void {
    // eslint-disable-next-line no-console
    console.info('ERROR');
    // eslint-disable-next-line no-console
    console.log(err);
    process.exit(1);
});

request.end();


/* eslint-enable unicorn/no-process-exit */
