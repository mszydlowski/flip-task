import {connectToDb} from './db/dbConnect';
import {logger, createMorganMiddleware} from './utils/logger';
import {PORT} from './core/services/configService';
import {initializeDb} from './db/initialize';
import * as express from 'express';
import cartRoutes from './http/routes/cartRoutes';
import productRoutes from './http/routes/productRoutes';
import authRoutes from './http/routes/authRoutes';
import healthcheckRoute from './http/routes/healthcheckRoute';
import * as bodyParser from 'body-parser';
import {CustomException} from './error/CustomException';
import {ErrorMessages} from './error/ErrorMessages';

const app = express();

function configRoutes(): void {
    cartRoutes(app);
    productRoutes(app);
    authRoutes(app);
    healthcheckRoute(app);
}

connectToDb().then(async () => {
    createMorganMiddleware(app);
    await initializeDb();
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    configRoutes();
    app.use(express.static('apidoc/'));
    app.listen(PORT, () => logger.info(`Application is running on port ${PORT}`));
}).catch((error) => {
    logger.error(`Failed to connect to DB. Reason: ${error.message} ${error.stack}`);
    throw new CustomException(ErrorMessages.couldNotConnectToDb);
});