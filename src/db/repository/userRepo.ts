import {User} from '../models/User';
import {generate} from 'password-hash';

export async function registerUser(login: string, password: string): Promise<void> {
    await User.create({login, password: generate(password, {saltLength: 8})});
}

export async function findUserByLogin(login: string): Promise<{login: string, password: string}> {
    return User.findOne({login}).lean();
}