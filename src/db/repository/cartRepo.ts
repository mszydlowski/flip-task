import {Cart} from '../models/Cart';
import {Document} from 'mongoose';
import {CartProduct} from '../../core/businessEntities/CartProduct';

export async function addCart(): Promise<Document> {
    const cart = new Cart();
    return cart.save();
}

export async function getCartById(id: string): Promise<Document> {
    return Cart.findById(id).lean();
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export async function getCartByIdWithProducts(id: string): Promise<any> {
    return Cart.findById(id).lean().populate('products.productId');
}

export async function updateProductData(cartId: string, productData: CartProduct[]): Promise<void> {
    return Cart.updateOne({_id: cartId}, {products: productData});
}