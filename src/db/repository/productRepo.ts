import {Product} from '../models/Product';
import {RawProduct} from '../../core/businessEntities/RawProduct';

/* eslint-disable @typescript-eslint/no-explicit-any */

export async function saveProduct(rawProduct: RawProduct): Promise<void> {
    const newProduct = new Product(rawProduct);
    await newProduct.save();
}

export async function findProductByName(name: string): Promise<any> {
    return Product.findOne({name});
}

export async function findProductsByIds(ids: string[]): Promise<any[]> {
    return Product.find({_id: {$in: ids}}).lean();
}

interface BulkIncrementUpdateItem {
    id: string;
    updateQuery: object
}

function buildIncrementObject(input: object): {$inc: object} {
    return {
        $inc: input
    };
}

export async function bulkIncrement(items: BulkIncrementUpdateItem[]): Promise<void> {
    await Product.bulkWrite(items.map((item) => ({
        updateOne: {
            filter: {_id: item.id},
            update: buildIncrementObject(item.updateQuery)
        }
    })));
}

export async function findAllProductsWhere(condition: string): Promise<any[]> {
    return Product.find({}).$where(condition).select('-frozenQuantity');
}

/* eslint-enable @typescript-eslint/no-explicit-any */
