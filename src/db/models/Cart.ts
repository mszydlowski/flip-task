import {Schema, model, Types} from 'mongoose';
import * as mongooseInteger from 'mongoose-integer';
import {Models} from '../models';

const CartSchema = new Schema({
    products: [{
        productId: {
            type: Schema.Types.ObjectId,
            ref: Models.Product
        },
        quantity: Number
    }]
});

CartSchema.plugin(mongooseInteger);


export const Cart = model('Cart', CartSchema);