import {Schema, model} from 'mongoose';
import * as mongooseInteger from 'mongoose-integer';

export const ProductSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        sparse: true
    },
    price: {
        currency: {
            type: String,
            required: true
        },
        value: {
            type: Number,
            required: true
        }
    },
    quantity: {
        type: Number,
        required: true,
        integer: true
    },
    frozenQuantity: {
        type: Number,
        required: true,
        default: 0,
        integer: true
    },
    description: {
        type: String,
        maxlength: 300
    }
});


ProductSchema.plugin(mongooseInteger);

export const Product = model('Product', ProductSchema);