import * as mongoose from 'mongoose';
import {DB_HOST, DB_NAME, DB_PROTOCOL, DB_PORT} from '../core/services/configService';

export async function connectToDb(): Promise<typeof mongoose> {
    return mongoose.connect(`${DB_PROTOCOL}://${DB_HOST}:${DB_PORT}/${DB_NAME}`,
        {useNewUrlParser: true, useUnifiedTopology: true});
}