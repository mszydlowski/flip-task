import {RawProduct} from '../core/businessEntities/RawProduct';
import {saveProductIfNotExists} from '../core/dataServices/productDataService';

const sampleProducts: RawProduct[] = [
    {
        name: 'White T-Shirt',
        price: {
            currency: 'EUR',
            value: 30.45
        },
        quantity: 10
    },
    {
        name: 'Black T-Shirt',
        price: {
            currency: 'EUR',
            value: 10.2
        },
        quantity: 4
    },
    {
        name: 'Yellow Pants',
        price: {
            currency: 'USD',
            value: 20
        },
        quantity: 7
    }
];


async function initializeProducts(): Promise<void> {
    sampleProducts.forEach(async (product) => {
        await saveProductIfNotExists(product);
    });
}

export async function initializeDb(): Promise<void> {
    await initializeProducts();
}