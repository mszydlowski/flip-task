import * as nconf from 'nconf';
import * as path from 'path';


const nconfFolder = path.join(__dirname, '../../../src/config/', `${process.env.NODE_ENV || 'dev'}.json`);

export const config = nconf.argv()
    .env()
    .file(nconfFolder)
    .defaults({

    });
