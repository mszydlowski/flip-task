import {isValidObjectId} from 'mongoose';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function isValidCartProductList (rawProductList: any): boolean {
    if (!Array.isArray(rawProductList)) {
        return false;
    }
    return rawProductList.every((item) => isValidObjectId(item.productId) && Number.isInteger(item.quantity) && item.quantity > 0);
}