import {body, param, ValidationChain} from 'express-validator';
import {isValidCartProductList} from './fieldValidators';
import {Types} from 'mongoose';

export const validateAddRemoveProductsToCart: ValidationChain[] = [
    param('id')
        .custom(Types.ObjectId.isValid.bind(this))
        .withMessage('Invalid cart ID'),
    body('products')
        .custom(isValidCartProductList)
        .withMessage('Product list must be an array of objects with productId and positive integer quantity fields!')
];

export const validateCheckout: ValidationChain[] = [
    param('id')
        .custom(Types.ObjectId.isValid.bind(this))
        .withMessage('Invalid cart ID'),
];