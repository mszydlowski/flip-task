import {body, param, ValidationChain} from 'express-validator';


const checkPassword = function(password: string): boolean{
    return !!password.match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[ !#$%&()*+=?@^])[\d !#$%&()*+=?@A-Z^a-z]{8,}$/);
};

export const validateLoginRegister: ValidationChain[] = [
    body('login')
        .isString()
        .withMessage('Invalid login'),
    body('password')
        .custom(checkPassword.bind(this))
        .withMessage('Invalid password, must be at least 8 chars long including min 1 number, 1 letter and 1 special char'),
];