import {Request, Application, Response} from 'express';
import * as httpStatusCodes from 'http-status-codes';
import {handleError} from '../../error/errorHandler';
import {findAllAvailableProducts} from '../../core/dataServices/productDataService';


async function getAllAvailableProducts(req: Request, res: Response): Promise<Response> {
    return res.status(httpStatusCodes.OK).json({availableProducts: await findAllAvailableProducts()});
}

export default (app: Application): void => {

    /**
     * @api {get} /products/available Get a list of available products
     * @apiGroup Product
     *
     * @apiDescription Add products to an existing cart
     *
     * @apiExample {curl} Example usage:
     *      curl -X GET http://localhost:3010/products/available
     * @apiParamExample {json} Request-Example:
     *     {
     *        "availableProducts": [
     *            {
     *                "price": {
     *                    "currency": "EUR",
     *                    "value": 30.45
     *                },
     *                "frozenQuantity": 0,
     *                "_id": "5e9f59c9fb49d60b32be753f",
     *                "name": "White T-Shirt",
     *                "quantity": 10,
     *                "__v": 0
     *            },
     *            {
     *                "price": {
     *                    "currency": "EUR",
     *                    "value": 10.2
     *                },
     *                "frozenQuantity": 1,
     *                "_id": "5e9f59c9fb49d60b32be7541",
     *                "name": "Black T-Shirt",
     *                "quantity": 4,
     *                "__v": 0
     *            }
     *        ]
     *    }
     *
     *
     */
    app.get('/products/available', handleError(getAllAvailableProducts));
};
