import {Request, Application, Response} from 'express';
import * as httpStatusCodes from 'http-status-codes';
import {addEmptyCart, checkCartExists} from '../../core/dataServices/cartDataService';
import {validateAddRemoveProductsToCart, validateCheckout} from '../validators/cartValidators';
import {handleError} from '../../error/errorHandler';
import {CustomException} from '../../error/CustomException';
import {ErrorMessages} from '../../error/ErrorMessages';
import {checkProductsExistInDesiredQuantities, groupProductsById, freezeSelectedProductQuantities, checkProductsExistInCart} from '../../core/services/productService';
import {addNewProductsToCart, removeProductsFromCart, calculateCheckout} from '../../core/services/cartService';
import {auth} from '../../core/services/authService';

async function addCart(req: Request, res: Response): Promise<Response> {
    const {_id: _id} = await addEmptyCart();
    return res.status(httpStatusCodes.OK).json({_id});
}

async function addProductsToCart(req: Request, res: Response): Promise<Response> {
    if (!(await checkCartExists(req.params.id))) {
        return res.status(httpStatusCodes.NOT_FOUND).json({message: ErrorMessages.noSuchCart});
    }
    const groupedProducts = groupProductsById(req.body.products);
    await checkProductsExistInDesiredQuantities(groupedProducts);
    await addNewProductsToCart(req.params.id, groupedProducts);
    return res.status(httpStatusCodes.OK).json({message: 'Products successfully added to cart!'});
}

async function removeFromCart(req: Request, res: Response): Promise<Response> {
    if (!(await checkCartExists(req.params.id))) {
        return res.status(httpStatusCodes.NOT_FOUND).json({message: ErrorMessages.noSuchCart});
    }
    const groupedProducts = groupProductsById(req.body.products);
    await checkProductsExistInCart(req.params.id, groupedProducts);
    await removeProductsFromCart(req.params.id, groupedProducts);
    return res.status(httpStatusCodes.OK).json({message: 'Products successfully removed from cart!'});
}

async function checkout(req: Request, res: Response): Promise<Response> {
    if (!(await checkCartExists(req.params.id))) {
        return res.status(httpStatusCodes.NOT_FOUND).json({message: ErrorMessages.noSuchCart});
    }
    return res.status(httpStatusCodes.OK).json(await calculateCheckout(req.params.id, req.params.currency));
}


export default (app: Application): void => {
    /**
     * @api {post} /cart Create cart
     * @apiGroup Cart
     *
     * @apiDescription Create a new empty cart to add products into.
     *
     * @apiHeader {String} Authorization Token to pass (format: Bearer your-token)
     *
     * @apiHeaderExample Header-Example:
     *
     * Bearer ey32idj82jdjqwujdq9w8dj209qjdiqj32d982j9d2j92djd2
     *
     * @apiExample {curl} Example usage:
     *      curl -X POST http://localhost:3010/cart
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK
     * {
     *      _id: "5e9c9239a7578c06eaa367d8"
     * }
     *
     */
    app.post('/cart', auth(), handleError(addCart));

    /**
     * @api {put} /cart/:id/add-products Add products to cart
     * @apiGroup Cart
     *
     * @apiDescription Add products to an existing cart
     *
     * @apiHeader {String} Authorization Token to pass (format: Bearer your-token)
     *
     * @apiHeaderExample Header-Example:
     *
     * Bearer ey32idj82jdjqwujdq9w8dj209qjdiqj32d982j9d2j92djd2
     *
     * @apiParam (path) {ObjectId} id ID of the cart to add to (must be a 24-character hexadecimal string)
     * @apiParam (body) {json} products A JSON list of objects having productId and quantity
     * @apiExample {curl} Example usage:
     *      curl -X PUT http://localhost:3010/cart/5e9c9239a7578c06eaa367d8/add-products
     * @apiParamExample {json} Request-Example:
     * {
     *     "products": [{
     *         "productId": "5e9c592b94c13002f38ab77e",
     *         "quantity": 2
     *     }, {
     *         "productId": "5e9ca530b6cd3207404314d3",
     *         "quantity": 3
     *     }]
     * }
     *
     * @apiError noSuchCart No cart by a given ID
     * @apiError noSuchProduct No product by a given ID
     * @apiError productsNotAvailableInThatAmount One or more products are not available in that amount
     * @apiError errorAddingToCart An error occured while adding products to cart
     *
     */
    app.put('/cart/:id/add-products', auth(), validateAddRemoveProductsToCart, handleError(addProductsToCart));

    /**
     * @api {put} /cart/:id/remove-products Remove products from cart
     * @apiGroup Cart
     *
     * @apiDescription Remove products from an existing cart
     *
     * @apiHeader {String} Authorization Token to pass (format: Bearer your-token)
     *
     * @apiHeaderExample Header-Example:
     *
     * Bearer ey32idj82jdjqwujdq9w8dj209qjdiqj32d982j9d2j92djd2
     *
     * @apiParam (path) {ObjectId} id ID of the cart to remove from (must be a 24-character hexadecimal string)
     * @apiParam (body) {json} products A JSON list of objects having productId and quantity
     * @apiExample {curl} Example usage:
     *      curl -X PUT http://localhost:3010/cart/5e9c9239a7578c06eaa367d8/remove-products
     * @apiParamExample {json} Request-Example:
     * {
     *     "products": [{
     *         "productId": "5e9c592b94c13002f38ab77e",
     *         "quantity": 2
     *     }, {
     *         "productId": "5e9ca530b6cd3207404314d3",
     *         "quantity": 3
     *     }]
     * }
     *
     * @apiError noSuchCart No cart by a given ID
     * @apiError noSuchProduct No product by a given ID
     * @apiError errorAddingToCart An error occured while removing products from cart
     *
     */
    app.put('/cart/:id/remove-products', auth(), validateAddRemoveProductsToCart, handleError(removeFromCart));

    /**
     * @api {get} /cart/:id/checkout Get checkout info
     * @apiGroup Cart
     *
     * @apiDescription Get checkout info for cart and selected currency
     *
     * @apiHeader {String} Authorization Token to pass (format: Bearer your-token)
     *
     * @apiHeaderExample Header-Example:
     *
     * Bearer ey32idj82jdjqwujdq9w8dj209qjdiqj32d982j9d2j92djd2
     *
     * @apiExample {curl} Example usage:
     *      curl -X GET http://localhost:3010/cart/5e9c9239a7578c06eaa367d8/checkout/PLN
     *
     * @apiError noSuchCart No cart by a given ID
     *
     * @apiSuccessExample {json} Success-Response:
     * [
     *    {
     *        "_id": "5e9f67aa2beee21289d71cd1",
     *        "name": "White T-Shirt",
     *        "quantity": 10,
     *        "originalCurrency": "EUR",
     *        "priceInOriginalCurrency": 30.45,
     *        "priceInEUR": "30.45",
     *        "exchangeRate": 1
     *    },
     *    {
     *        "_id": "5e9f67aa2beee21289d71cd3",
     *        "name": "Black T-Shirt",
     *        "quantity": 4,
     *        "originalCurrency": "EUR",
     *        "priceInOriginalCurrency": 10.2,
     *        "priceInEUR": "10.20",
     *        "exchangeRate": 1
     *    },
     *   {
     *        "_id": "5e9f67aa2beee21289d71cd2",
     *        "name": "Yellow Pants",
     *        "quantity": 7,
     *        "originalCurrency": "USD",
     *        "priceInOriginalCurrency": 20,
     *        "priceInEUR": "18.46",
     *        "exchangeRate": 1.0837
     *    }
     * ]
     *
     */
    app.get('/cart/:id/checkout/:currency', auth(), validateCheckout, handleError(checkout));
};
