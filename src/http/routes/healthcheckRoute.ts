import * as express from 'express';
import {OK} from 'http-status-codes';
import {healthcheckUrl} from '../urls';

const okResponse = {msg: 'OK'};

export default function healthcheckRoute(app: express.Application): void {
    app.get(healthcheckUrl, (req, res) => res.status(OK).json(okResponse));
}