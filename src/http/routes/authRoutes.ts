import {Request, Application, Response} from 'express';
import * as httpStatusCodes from 'http-status-codes';
import {addEmptyCart, checkCartExists} from '../../core/dataServices/cartDataService';
import {validateAddRemoveProductsToCart, validateCheckout} from '../validators/cartValidators';
import {handleError} from '../../error/errorHandler';
import {CustomException} from '../../error/CustomException';
import {ErrorMessages} from '../../error/ErrorMessages';
import {checkProductsExistInDesiredQuantities, groupProductsById, freezeSelectedProductQuantities, checkProductsExistInCart} from '../../core/services/productService';
import {addNewProductsToCart, removeProductsFromCart, calculateCheckout} from '../../core/services/cartService';
import {registerUser, checkCredentialsAndGetToken} from '../../core/dataServices/userDataService';
import {validateLoginRegister} from '../validators/authValidators';

async function register(req: Request, res: Response): Promise<Response> {
    await registerUser(req.body.login, req.body.password);
    return res.status(httpStatusCodes.OK).json({message: 'User successfully registered'});
}

async function login(req: Request, res: Response): Promise<Response> {
    const jwt = await checkCredentialsAndGetToken(req.body.login, req.body.password);
    return res.status(httpStatusCodes.OK).json({jwt});
}

export default (app: Application): void => {

    /**
     * @api {post} /register Add new user
     * @apiGroup Auth
     *
     * @apiDescription Registers a new user
     *
     * @apiParam (body) String login User login
     * @apiParam (body) String password User password (at least 8 chars including at least 1 number, 1 letter and 1 special character)
     * @apiExample {curl} Example usage:
     *      curl -X POST http://localhost:3010/register
     * @apiParamExample {json} Request-Example:
     * {
     *     "login": "foo",
     *     "password": "Password1!"
     * }
     *
     */
    app.post('/register', validateLoginRegister, handleError(register));


    /**
     * @api {post} /login Login
     * @apiGroup Auth
     *
     * @apiDescription Returns a JWT to authorize other requests
     *
     * @apiParam (body) String login User login
     * @apiParam (body) String password User password (at least 8 chars including at least 1 number, 1 letter and 1 special character)
     * @apiExample {curl} Example usage:
     *      curl -X POST http://localhost:3010/login
     * @apiParamExample {json} Request-Example:
     * {
     *     "login": "foo",
     *     "password": "Password1!"
     * }
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *     "jwt": "token"
     * }
     *
     */
    app.post('/login', validateLoginRegister, handleError(login));

};
