import {CartProduct} from './CartProduct';

export interface Cart {
    _id: string;
    products: CartProduct[]
}