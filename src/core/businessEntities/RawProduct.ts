
export interface RawProduct {
    name: string;
    price: {
        currency: string;
        value: number;
    }
    quantity: number;
    description?: string;
}