
export interface CheckoutProduct {
    _id: string;
    name: string;
    quantity: number;
    originalCurrency: string;
    retailPriceInOriginalCurrency: string;
    retailPriceInTargetCurrency: string;
    totalPriceInOriginalCurrency: string;
    totalPriceInTargetCurrency: string;
    exchangeRateOriginalToEUR: number;
    exchangeRateEURToTarget: number;

}

