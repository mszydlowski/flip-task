
export interface Product {
    _id: string;
    name: string;
    price: {
        currency: string;
        value: number;
    }
    quantity: number;
    frozenQuantity: number;
    description?: string;
}