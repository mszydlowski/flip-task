import {saveProduct, findProductByName, findProductsByIds, bulkIncrement, findAllProductsWhere} from '../../db/repository/productRepo';
import {RawProduct} from '../businessEntities/RawProduct';
import {Product} from '../businessEntities/Product';
import {CartProduct} from '../businessEntities/CartProduct';

export async function saveProductIfNotExists(rawProduct: RawProduct): Promise<void> {
    const product = await findProductByName(rawProduct.name);
    if (!product) {
        await saveProduct(rawProduct);
    }
}

export async function findProductsWithIds(productIds: string[]): Promise<Product[]> {
    const dbProducts = (await findProductsByIds(productIds));
    return dbProducts.map((dbProduct) => ({
        ...dbProduct,
        // eslint-disable-next-line no-underscore-dangle
        _id: dbProduct._id.toString()
    }));
}

export async function updateFrozenQuantities(cartProducts: CartProduct[], decrement?: boolean): Promise<void> {
    const flipFactor = -1;
    const writes = cartProducts.map((item) => ({
        id: item.productId,
        updateQuery: {frozenQuantity: item.quantity * (decrement ? flipFactor : 1)}
    }));
    await bulkIncrement(writes);
}

export async function findAllAvailableProducts(): Promise<Product[]> {
    return findAllProductsWhere('this.frozenQuantity < this.quantity');
}