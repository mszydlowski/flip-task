import * as userRepo from '../../db/repository/userRepo';
import {CustomException} from '../../error/CustomException';
import {ErrorMessages} from '../../error/ErrorMessages';
import {NOT_FOUND, UNAUTHORIZED} from 'http-status-codes';
import {verify} from 'password-hash';
import {sign} from 'jsonwebtoken';
import {JWT_SECRET, JWT_EXPIRE_TIME} from '../services/configService';

export async function registerUser(login: string, password: string): Promise<void> {
    await userRepo.registerUser(login, password);
}

export async function checkCredentialsAndGetToken(login: string, password: string): Promise<string> {
    const user = await userRepo.findUserByLogin(login);
    if (!user) {
        throw new CustomException(ErrorMessages.userNotFound, NOT_FOUND);
    } if (!verify(password, user.password)) {
        throw new CustomException(ErrorMessages.invalidPassword, UNAUTHORIZED);
    }
    return sign({login}, JWT_SECRET, {expiresIn: JWT_EXPIRE_TIME});
}

export async function findUserByLogin(login: string): Promise<{login: string, password: string}> {
    return userRepo.findUserByLogin(login);
}