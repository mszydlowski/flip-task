import * as cartRepo from '../../db/repository/cartRepo';
import {Cart} from '../businessEntities/Cart';
import {CartProduct} from '../businessEntities/CartProduct';

export async function addEmptyCart(): Promise<Cart> {
    const cart = (await cartRepo.addCart()).toJSON();
    return {
        // eslint-disable-next-line no-underscore-dangle
        _id: cart._id,
        products: cart.products
    };
}

export async function checkCartExists(id: string): Promise<boolean> {
    return !!(await cartRepo.getCartById(id));
}

export async function getCartById(id: string, withProducts?: boolean): Promise<Cart> {
    return withProducts ? cartRepo.getCartByIdWithProducts(id) : cartRepo.getCartById(id);
}

export async function updateProductData(cartId: string, newCartProducts: CartProduct[]): Promise<void> {
    await cartRepo.updateProductData(cartId, newCartProducts);
}