
import Axios from 'axios';
import {CustomException} from '../../error/CustomException';
import {ErrorMessages} from '../../error/ErrorMessages';

const CURRENCY_URL = 'https://api.exchangeratesapi.io/latest';


interface CurrencyResponse {
    rates: {
        [field: string]: number;
    }
}

export async function getCurrencyData(): Promise<CurrencyResponse> {
    try {
        return (await Axios.get(CURRENCY_URL)).data;
    }catch (error) {
        throw new CustomException(ErrorMessages.failedToGetCurrencyInfo);
    }
}