import {freezeSelectedProductQuantities, unfreezeSelectedProductQuantities} from './productService';
import {CartProduct} from '../businessEntities/CartProduct';
import {updateProductData, getCartById} from '../dataServices/cartDataService';
import {CustomException} from '../../error/CustomException';
import {ErrorMessages} from '../../error/ErrorMessages';
import * as _ from 'lodash';
import {getCartByIdWithProducts} from '../../db/repository/cartRepo';
import {getCurrencyData} from './currencyService';
import {CheckoutProduct} from '../businessEntities/Checkout';

function mergeProductData(existingProductData: CartProduct[], newProductData: CartProduct[]): CartProduct[] {
    const allProductData = existingProductData.concat(newProductData);
    return _(allProductData).groupBy('productId').map((value, key) => ({
        productId: key,
        quantity: _.sumBy(value, (item) => item.quantity)
    })).value();
};

function filterEmptyCartProducts(cartProducts: CartProduct[]): CartProduct[] {
    return cartProducts.filter((product) => product.quantity > 0);
}

export async function addNewProductsToCart(cartId: string, newCartProducts: CartProduct[]): Promise<void> {
    // This should be done using transactions, but that would require to run the server as a replica set,
    // so I just rollback manually.
    let rollbackFreeze = false;
    try {
        await freezeSelectedProductQuantities(newCartProducts);
        rollbackFreeze = true;
        const cart = await getCartById(cartId);
        const newProductData = mergeProductData(cart.products, newCartProducts);
        await updateProductData(cartId, newProductData);
    } catch (error) {
        if (rollbackFreeze) {
            await unfreezeSelectedProductQuantities(newCartProducts);
        }
        throw new CustomException(ErrorMessages.errorAddingToCart);
    }
}

function flipQuantities(cartProducts: CartProduct[]): CartProduct[] {
    const flipFactor = -1;
    return cartProducts.map((cartProduct) => ({
        productId: cartProduct.productId,
        quantity: flipFactor * cartProduct.quantity
    }));
}

export async function removeProductsFromCart(cartId: string, removedCartProducts: CartProduct[]): Promise<void> {
    // This should be done using transactions, but that would require to run the server as a replica set,
    // so I just rollback manually.
    let rollbackUnfreeze = false;
    try {
        await unfreezeSelectedProductQuantities(removedCartProducts);
        rollbackUnfreeze = true;
        const cart = await getCartById(cartId);
        const newProductData = mergeProductData(cart.products, flipQuantities(removedCartProducts));
        await updateProductData(cartId, filterEmptyCartProducts(newProductData));
    } catch (error) {
        if (rollbackUnfreeze) {
            await freezeSelectedProductQuantities(removedCartProducts);
        }
        throw new CustomException(ErrorMessages.errorRemovingFromCart);
    }
}

export async function calculateCheckout(cartId: string, targetCurrency: string): Promise<CheckoutProduct[]> {
    const cart = await getCartByIdWithProducts(cartId);
    const currencyData = await getCurrencyData();
    const decimalDigits = 2;
    return cart.products.map((cartProduct) => {
        const product = cartProduct.productId;
        const exchangeRateOriginalToEUR = product.price.currency === 'EUR' ? 1 : currencyData.rates[product.price.currency];
        const exchangeRateEURToTarget = targetCurrency === 'EUR' ? 1 : currencyData.rates[targetCurrency];
        const retailPriceInEUR = exchangeRateOriginalToEUR ? (product.price.value / exchangeRateOriginalToEUR) : 0;
        const totalPriceInEUR = exchangeRateOriginalToEUR ? (product.price.value * cartProduct.quantity / exchangeRateOriginalToEUR) : 0;

        return {
            // eslint-disable-next-line no-underscore-dangle
            _id: product._id,
            name: product.name,
            quantity: cartProduct.quantity,
            originalCurrency: product.price.currency,
            totalPriceInOriginalCurrency: (product.price.value * cartProduct.quantity).toFixed(decimalDigits),
            totalPriceInTargetCurrency: (totalPriceInEUR * exchangeRateEURToTarget).toFixed(decimalDigits),
            retailPriceInOriginalCurrency: product.price.value.toFixed(decimalDigits),
            retailPriceInTargetCurrency: (retailPriceInEUR * exchangeRateEURToTarget).toFixed(decimalDigits),
            exchangeRateOriginalToEUR,
            exchangeRateEURToTarget
        };
    });
}