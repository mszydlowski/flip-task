import {CartProduct} from '../businessEntities/CartProduct';
import * as _ from 'lodash';
import {findProductsWithIds, updateFrozenQuantities} from '../dataServices/productDataService';
import {CustomException} from '../../error/CustomException';
import {ErrorMessages} from '../../error/ErrorMessages';
import {Product} from '../businessEntities/Product';
import {getCartById} from '../dataServices/cartDataService';

export function groupProductsById(newCartProducts: CartProduct[]): CartProduct[] {
    return _(newCartProducts).groupBy('productId').map((value, key) => ({
        productId: key,
        quantity: _.sumBy(value, (item) => item.quantity)
    })).value();
}

function checkProductsHaveDesiredQuantities(newCartProducts: CartProduct[], dbProducts: Product[]): boolean {
    return newCartProducts.every(((newCartProduct): boolean => {
        // eslint-disable-next-line no-underscore-dangle
        const dbProduct = dbProducts.find(((item): boolean => item._id === newCartProduct.productId));
        const availableAmount = dbProduct.quantity - dbProduct.frozenQuantity;
        return newCartProduct.quantity <= availableAmount;
    }));
}

export async function checkProductsExistInDesiredQuantities(newCartProducts: CartProduct[]): Promise<void> {
    const productIds = newCartProducts.map((item) => item.productId);
    const products = await findProductsWithIds(productIds);
    if (productIds.length !== products.length) {
        throw new CustomException(ErrorMessages.noSuchProduct);
    }
    if (!checkProductsHaveDesiredQuantities(newCartProducts, products)) {
        throw new CustomException(ErrorMessages.productsNotAvailableInThatAmount);
    }
}

export async function checkProductsExistInCart(cartId: string, cartProducts: CartProduct[]): Promise<void> {
    const productIds = cartProducts.map((item) => item.productId.toString());
    const cart = await getCartById(cartId);
    const cartProductIds = cart.products.map((item) => item.productId.toString());
    const areProductsInCart = _.every(productIds, (productId) => cartProductIds.includes(productId));
    if (!areProductsInCart) {
        throw new CustomException(ErrorMessages.productsNotInCart);
    }
}

export async function freezeSelectedProductQuantities(newCartProducts: CartProduct[]): Promise<void> {
    await updateFrozenQuantities(newCartProducts);
}

export async function unfreezeSelectedProductQuantities(newCartProducts: CartProduct[]): Promise<void> {
    await updateFrozenQuantities(newCartProducts, true);
}
