import * as expressJwt from 'express-jwt';
import {JWT_SECRET} from '../services/configService';
import {Request, Response, NextFunction} from 'express';
import {findUserByLogin} from '../dataServices/userDataService';
import {CustomException} from '../../error/CustomException';
import {ErrorMessages} from '../../error/ErrorMessages';
import {UNAUTHORIZED} from 'http-status-codes';


interface AuthReq extends Request {
    user: {
        login: string;
    };
}

async function checkIfUserWithLoginExists(req: AuthReq, res: Response, next: NextFunction): Promise<void> {
    const user = await findUserByLogin(req.user.login);
    if (!user || user.login !== req.user.login) {
        throw new CustomException(ErrorMessages.incorrectCredentials, UNAUTHORIZED);
    }
    return next();
}

export function auth(): [expressJwt.RequestHandler, (req: AuthReq, res: Response, next: NextFunction) => Promise<void>] {
    return [expressJwt({secret: JWT_SECRET}), checkIfUserWithLoginExists];
}

