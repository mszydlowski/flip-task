import {config} from '../../config/config';


export const PORT = config.get('PORT');
export const DB_PROTOCOL = config.get('DB_PROTOCOL');
export const DB_HOST = config.get('DB_HOST');
export const DB_NAME = config.get('DB_NAME');
export const DB_PORT = config.get('DB_PORT');
export const JWT_SECRET = config.get('JWT_SECRET');
export const JWT_EXPIRE_TIME = config.get('JWT_EXPIRE_TIME');