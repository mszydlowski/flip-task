
export const ErrorMessages = {
    noSuchCart: 'No cart by a given ID',
    noSuchProduct: 'No product by a given ID',
    productsNotAvailableInThatAmount: 'One or more products are not available in that amount',
    errorAddingToCart: 'An error occured while adding products to cart',
    errorRemovingFromCart: 'An error occured while removing products from cart',
    productsNotInCart: 'One or more products are not in the selected cart',
    couldNotConnectToDb: 'DB connection failed',
    failedToGetCurrencyInfo: 'Could not retrieve currency info from server',
    userNotFound: 'User by that login not found',
    invalidPassword: 'Invalid password',
    incorrectCredentials: 'Invalid credentials'
};