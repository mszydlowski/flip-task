import {RequestHandler, Request, Response, NextFunction} from 'express';
import {validationResult} from 'express-validator';
import {logger} from '../utils/logger';
import * as httpStatusCodes from 'http-status-codes';
import {CustomException} from './CustomException';

export function handleError(handler: RequestHandler): RequestHandler {
    return async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            logger.info(errors.array());
            return res.status(httpStatusCodes.BAD_REQUEST).json({
                errors: errors.array()
            });
        }
        try {
            await handler(req, res, next);
        } catch (error) {
            logger.error(error.toString());
            if (error instanceof CustomException) {
                res.status(error.code).json(error.message);
            } else {
                res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
            }
        }
    };
}