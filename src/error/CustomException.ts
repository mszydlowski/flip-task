import * as httpStatusCodes from 'http-status-codes';

export class CustomException extends Error {
    readonly message: string;
    readonly code: number;
    readonly error: Error | undefined;

    constructor(message: string, code: number = httpStatusCodes.INTERNAL_SERVER_ERROR, error?: Error) {
        super();
        this.message = message;
        this.code = code;
        this.error = error;
    }

    public toString(): string {
        return `${this.message}\n${this.error ? this.error.stack : ''}`;
    }
}