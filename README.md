# flip-task

Recruitment task for Flip


## Prerequisites

You need to have Docker and docker-compose installed on your system. If you want to build a developent image using the provided
shell script, you also need to be able to run bash scripts. 


## Setup

By default, the application will run in a lightweight, production mode. The production configs are the same as development 
ones (local), this is only done as a proof of concept of the application suited for production use, with no unnecessary 
development dependencies and scripts. To run:

```
docker-compose up --build
```

The image should build automatically. MongoDB is populated with several sample products to work on. 

If you want, you can build a development image of the rest api using `buildImage.sh` script, and then uncomment the commented `rest-api-debug` part in `docker-compose.yml` and comment the `rest-api` part. The app will then run in debug mode and will rebuild on any change.

## Tests

To run tests, execute:

```
npm run test-unit
```

## Documentation

Run:

```
npm run apidoc
```

and either view `index.html` from `apidoc` folder in browser, or if the application is up, go to http://localhost:3010/index.html.


