################################# Development stage ####################################

FROM node:12.16.2-alpine as development

ADD --chown=node package.json package-lock.json nodemon.json tsconfig.json /app/

WORKDIR /app

RUN npm install

COPY --chown=node src src

RUN npm run build

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.4.0/wait /wait
RUN chmod +x /wait

CMD /wait && npm run debug

################################# Production stage ######################################

FROM node:10.15.3-alpine as production

WORKDIR /app

RUN chown -R node:node /app

COPY --chown=node --from=development /app/package*.json ./
COPY --chown=node --from=development /app/dist dist
COPY --chown=node --from=development /wait /wait
COPY --chown=node --from=development /app/src/config /app/src/config

RUN npm ci --production

ENV NODE_ENV production

## choose the right config and extract port for healthcheck
RUN grep -Eo '"PORT":.*?[^\\],' /app/src/config/${NODE_ENV}.json | tr -dc '0-9' >> port.txt

HEALTHCHECK --interval=30s --timeout=30s --start-period=30s --retries=15 CMD PORT=3010 node dist/src/utils/healthcheck.js

USER node

CMD /wait && npm run start

